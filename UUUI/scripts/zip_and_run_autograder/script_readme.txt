quick guide:
	1. create directory UUUI_labs/
	2. place zip_and_run_autograder.sh in that directory
	3. create directory for each lab (UUUI_labs/lab1, UUUI_labs/lab2, ...)
	4. download or move lab files from intranet in corresponding directory 
	(UUUI_labs/lab1/autograder/..., UUUI_labs/lab1/lab1_files/... , UUUI_labs/lab2/autograder_lab2/..., UUUI_labs/lab2/lab2_files/...)
	5. write/copy your solutions in templates/lab2py folder
	6. open command prompt and position yourself in .../UUUI_labs/
	7. run: "./zip_and_run_autograder.sh [your_jmbag] [num_of_lab_exercise_to_test]" (requires linux terminal)
	
example for testing solution for second lab with jmbag 003612345678:
	./zip_and_run_autograder.sh 003612345678 2