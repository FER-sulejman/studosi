#!/bin/sh
splitter="$(printf "%0.s-" $(seq 1 40))"  # prints 30 dashes(-)
echo "${splitter}Running script${splitter}"
echo "Script works only for python3 solutions."
echo "INFO:"
echo "If script does not recognise commands or some syntax error occurs one should run:"
echo '\ttr -d '\''\\''r'\'' < zip_and_run_autograder.sh > new_bash_file_fixed.sh'
echo "and then run the new bash."
echo "${splitter}${splitter}"
echo "INFO_2:"
echo "Script should be placed in upmost directory: (example)"
echo "\tUUUI_labs/zip_and_run_autograder.sh"
echo "\tUUUI_labs/lab1/autograder/..."
echo "\tUUUI_labs/lab1/lab1_files/..."
echo "\tUUUI_labs/lab2/autograder_lab2/..."
echo "\tUUUI_labs/lab2/lab2_files/..."
echo "\t..."
echo "where lab1_files, autograder, ... are directories downloaded from intranet"
echo "${splitter}${splitter}"
if [ $# -ne 2 ] ; then
    echo 'Script requires two argument: [JMBAG] [LAB_NUM]'
    exit 0
fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
LAB="lab${2}"
case $2 in
  1)
    LAB_POSTFIX=""
    ;;
  *)
    LAB_POSTFIX="_$LAB"
    ;;
esac
echo Current dir: "$DIR"
echo JMBAG: "$1"
echo "LAB: ${LAB}, ${LAB_POSTFIX}"
echo "${splitter}${splitter}"
mkdir -p ./"$LAB"/autograder"$LAB_POSTFIX"/solutions/"$1"
cd ./"$LAB"/"$LAB"_files/templates
echo "zipping..."
zip -r "$DIR"/"$LAB"/autograder"$LAB_POSTFIX"/solutions/"$1"/"$1".zip "$LAB"py
echo "zipped!"
echo "${splitter}${splitter}"
cd "$DIR"/"$LAB"/autograder"$LAB_POSTFIX"
python3 autograder.py --solutions solutions --test_suites test_suites"$LAB_POSTFIX" --evaluation_log full.log
notepad="$(which notepad++.exe)"
"$notepad" "full.log"
echo "${splitter}Ending script${splitter}"
